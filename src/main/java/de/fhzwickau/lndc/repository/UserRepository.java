package de.fhzwickau.lndc.repository;

import de.fhzwickau.lndc.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
