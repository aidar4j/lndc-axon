package de.fhzwickau.lndc.service;

import de.fhzwickau.lndc.dto.CreateUserDto;
import de.fhzwickau.lndc.view.UserView;

import java.util.List;

public interface UserService {
    UserView getById();

    List<UserView> getAll();

    void create(CreateUserDto createUserDto);
}
