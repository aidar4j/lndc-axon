package de.fhzwickau.lndc.service;

import de.fhzwickau.lndc.command.CreateUserCommand;
import de.fhzwickau.lndc.dto.CreateUserDto;
import de.fhzwickau.lndc.query.FindAllUsersQuery;
import de.fhzwickau.lndc.query.FindUserByIdQuery;
import de.fhzwickau.lndc.view.UserView;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DefaultUserService implements UserService {

    private final CommandGateway commandGateway;
    private final QueryGateway queryGateway;

    public DefaultUserService(CommandGateway commandGateway, QueryGateway queryGateway) {
        this.commandGateway = commandGateway;
        this.queryGateway = queryGateway;
    }

    @Override
    public void create(CreateUserDto createUserDto) {
        String userId = UUID.randomUUID().toString();

        CreateUserCommand command = new CreateUserCommand();
        command.setUserId(userId);
        command.setUsername(createUserDto.getUsername());
        command.setPassword(createUserDto.getPassword());

        commandGateway.send(command);
    }

    @Override
    public List<UserView> getAll() {
        return queryGateway.query(
                new FindAllUsersQuery(),
                ResponseTypes.multipleInstancesOf(UserView.class)
        ).join();
    }

    @Override
    public UserView getById() {
        return queryGateway.query(
                new FindUserByIdQuery(),
                ResponseTypes.instanceOf(UserView.class)
        ).join();
    }
}
