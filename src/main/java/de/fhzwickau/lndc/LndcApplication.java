package de.fhzwickau.lndc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LndcApplication {

	public static void main(String[] args) {
		SpringApplication.run(LndcApplication.class, args);
	}

}
