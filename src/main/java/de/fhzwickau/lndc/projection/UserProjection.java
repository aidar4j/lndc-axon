package de.fhzwickau.lndc.projection;

import de.fhzwickau.lndc.event.UserCreatedEvent;
import de.fhzwickau.lndc.query.FindAllUsersQuery;
import de.fhzwickau.lndc.view.UserView;

import java.util.List;

public interface UserProjection {
    void handle(UserCreatedEvent event);

    List<UserView> handle(FindAllUsersQuery findAllUsersQuery);
}
