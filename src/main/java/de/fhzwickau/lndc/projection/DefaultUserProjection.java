package de.fhzwickau.lndc.projection;

import de.fhzwickau.lndc.service.UserService;
import de.fhzwickau.lndc.view.UserView;
import de.fhzwickau.lndc.event.UserCreatedEvent;
import de.fhzwickau.lndc.model.User;
import de.fhzwickau.lndc.query.FindAllUsersQuery;
import de.fhzwickau.lndc.repository.UserRepository;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DefaultUserProjection implements UserProjection {

    @Autowired
    private UserRepository userRepository;

    @Override
    @EventHandler
    public void handle(UserCreatedEvent event) {
        User user = new User();
        user.setId(event.getUserId());
        user.setUsername(event.getUsername());
        user.setPassword(event.getPassword());

        userRepository.save(user);
    }

    @QueryHandler
    public List<UserView> handle(FindAllUsersQuery findAllUsersQuery) {
        List<User> users = userRepository.findAll();

        return users.stream()
                .map(it -> new UserView(it.getId(), it.getUsername(), it.getPassword()))
                .collect(Collectors.toList());
    }
}
