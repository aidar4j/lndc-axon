package de.fhzwickau.lndc.aggregate;

import de.fhzwickau.lndc.command.CreateUserCommand;
import de.fhzwickau.lndc.event.UserCreatedEvent;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate
@NoArgsConstructor
public class UserAggregate {

    @AggregateIdentifier
    private String userId;
    private String username;
    private String password;

    @CommandHandler
    public UserAggregate(CreateUserCommand command) {
        UserCreatedEvent event = new UserCreatedEvent(
                command.getUserId(),
                command.getUsername(),
                command.getPassword()
        );

        AggregateLifecycle.apply(event);
    }

    @EventSourcingHandler
    public void on(UserCreatedEvent event) {
        this.userId = event.getUserId();
        this.username = event.getUsername();
        this.password = event.getPassword();
    }
}
