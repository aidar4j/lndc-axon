package de.fhzwickau.lndc.controller;

import de.fhzwickau.lndc.dto.CreateUserDto;
import de.fhzwickau.lndc.view.UserView;
import de.fhzwickau.lndc.service.DefaultUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/users")
public class UserController {

    @Autowired
    private DefaultUserService defaultUserService;

    @PostMapping
    private void create(@RequestBody CreateUserDto createUserDto) {
        defaultUserService.create(createUserDto);
    }

    @GetMapping
    private List<UserView> getAll() {
        return defaultUserService.getAll();
    }

    @GetMapping("/{id}")
    private UserView getById(@PathVariable String id) {
        return defaultUserService.getById();
    }
}
