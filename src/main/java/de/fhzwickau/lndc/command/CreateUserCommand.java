package de.fhzwickau.lndc.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CreateUserCommand {

    @TargetAggregateIdentifier
    private String userId;
    private String username;
    private String password;
}
