package de.fhzwickau.lndc;

import de.fhzwickau.lndc.aggregate.UserAggregate;
import de.fhzwickau.lndc.command.CreateUserCommand;
import de.fhzwickau.lndc.event.UserCreatedEvent;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

class UserAggregateTest {

    private FixtureConfiguration<UserAggregate> fixture;

    @BeforeEach
    void setUp() {
        fixture = new AggregateTestFixture<>(UserAggregate.class);
    }
    String userId = UUID.randomUUID().toString();
    UserCreatedEvent event = new UserCreatedEvent(userId, "aidar4j", "password");

    @Test
    void handle_withCreateUserCommand_shouldProduceUserCreatedEvent() {
        String username = "aidar4j";
        String password = "password";

        fixture.givenNoPriorActivity()
                .when(new CreateUserCommand(userId, username, password))
                .expectEvents(event);
    }

}
